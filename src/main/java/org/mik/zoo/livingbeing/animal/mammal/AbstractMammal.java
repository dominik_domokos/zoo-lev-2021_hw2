package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractMammal extends AbstractAnimal implements Mammal {

    public static final String COL_HAIR_LENGTH="hair_length";

    private static List<Mammal> allMammal=new ArrayList<>();

    private int hairLength;

    public AbstractMammal() {
        super();
        if (!allMammal.contains(this))
            allMammal.add(this);
    }

    public AbstractMammal(String scientificName, String instanceName, String imageURI, 
                          int numberOfLegs, int numberOfTeeth, int weight, 
                          int hairLength) {
        super(scientificName, instanceName, imageURI, numberOfLegs, numberOfTeeth, weight, ReproductionType.OVUM);
        this.hairLength = hairLength;
    }

    public AbstractMammal(String scientificName) {
        super(scientificName, ReproductionType.OVUM);
    }

    public AbstractMammal(ResultSet rs) throws SQLException {
            super(rs);
            this.hairLength=rs.getInt(COL_HAIR_LENGTH);
            if (!allMammal.contains(this))
                allMammal.add(this);
    }

    @Override
    public int getHairLength() {
        return hairLength;
    }

    public void setHairLength(int hairLength) {
        this.hairLength = hairLength;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d",super.getUpdateSql(), COL_HAIR_LENGTH, getHairLength());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_HAIR_LENGTH);
        values.append(',').append(getHairLength());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractMammal)) return false;
        if (!super.equals(o)) return false;
        AbstractMammal that = (AbstractMammal) o;
        return hairLength == that.hairLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hairLength);
    }

    @Override
    public String toString() {
        return "AbstractMammal{" +
                super.toString() +
                "hairLength=" + hairLength +
                "} ";
    }

    public static List<Mammal> getAllMammal() {
        return allMammal;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_HAIR_LENGTH, COL_TYPE_INTEGER);
    }
}
