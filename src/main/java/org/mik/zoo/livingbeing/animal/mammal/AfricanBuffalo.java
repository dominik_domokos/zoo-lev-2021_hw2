package org.mik.zoo.livingbeing.animal.mammal;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AfricanBuffalo extends  AbstractMammal {

    public static final String SCIENTIFIC_NAME="Syncerus caffer";
    public static final int AVERAGE_WEIGTH=600;
    public static final int NUMBER_OF_TEETH=46;
    public static final int HAIR_LENGHT=5;
    public static final String IMAGE_URI="";

    public AfricanBuffalo() {
        super();
    }

    public AfricanBuffalo(String instanceName) {
        super(SCIENTIFIC_NAME, instanceName, IMAGE_URI, 4, NUMBER_OF_TEETH, AVERAGE_WEIGTH, HAIR_LENGHT);
    }

    public AfricanBuffalo(ResultSet rs) throws SQLException {
        super(rs);
    }

}
