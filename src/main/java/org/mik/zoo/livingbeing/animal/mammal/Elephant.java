package org.mik.zoo.livingbeing.animal.mammal;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Elephant extends AbstractMammal {

    public static final String TAG = "Elephant";
    public static final String SCIENTIFIC_NAME="Loxodonta africana";
    public static final int AVERAGE_WEIGTH=5000;
    public static final int NUMBER_OF_TEETH=26;
    public static final int HAIR_LENGTH=8;
    public static final String IMAGE_URI="";

    public Elephant() {
        super();
    }

    public Elephant(String instanceName) {
        super(SCIENTIFIC_NAME, instanceName, IMAGE_URI, 4, NUMBER_OF_TEETH, AVERAGE_WEIGTH, HAIR_LENGTH);
    }

    public Elephant(String instanceName, String imgUri) {
        super(SCIENTIFIC_NAME, instanceName, imgUri, 4, NUMBER_OF_TEETH, AVERAGE_WEIGTH, HAIR_LENGTH);
    }

    public Elephant(ResultSet rs) throws SQLException {
          super(rs);
    }

    public static Elephant factoryMethod(Scanner s) {
        String args[]=getArgs(s);
        return args.length != NUMBER_OF_CSV_ARGS ? null : new Elephant(args[0],
                    URI.create(args[1]).toString());
    }


}
