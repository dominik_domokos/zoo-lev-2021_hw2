package org.mik.zoo.livingbeing.plants.flowers;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Sunflower extends AbstractFlowers {

    public static final String SCIENTIFIC_NAME="Helianthus";
    public static final String FLOWER_COLOR="yellow";
    public static final int LEAF_NUM=5;
    public static final int AGE=3;
    public static final int Height=100;
    public static final String IMAGE_URI="";

    public Sunflower() {
        super();
    }

    public Sunflower(String instanceName) {
        super(SCIENTIFIC_NAME, instanceName, IMAGE_URI, FLOWER_COLOR, LEAF_NUM, AGE, Height);
    }

    public Sunflower(ResultSet rs) throws SQLException {
        super(rs);
    }

}
