package org.mik.zoo.livingbeing.plants.trees;

import org.mik.zoo.livingbeing.plants.flowers.AbstractFlowers;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Oaktree extends AbstractTrees {

    public static final String TAG = "Oaktree";
    public static final String SCIENTIFIC_NAME="Quercus";
    public static final int Thickness=100;
    public static final boolean Evergreen=true;
    public static final int AGE=40;
    public static final int Height=300;
    public static final String IMAGE_URI="";

    public Oaktree() {
        super();
    }

    public Oaktree(String instanceName) {
        super(SCIENTIFIC_NAME, instanceName, IMAGE_URI, Thickness, Evergreen, AGE, Height);
    }

    public Oaktree(String instanceName, String imgUri) {
        super(SCIENTIFIC_NAME, instanceName, imgUri, Thickness, Evergreen, AGE,Height);
    }

    public Oaktree(ResultSet rs) throws SQLException {
          super(rs);
    }

    public static Oaktree factoryMethod(Scanner s) {
        String args[]=getArgs(s);
        return args.length != NUMBER_OF_CSV_ARGS ? null : new Oaktree(args[0],
                    URI.create(args[1]).toString());
    }


}
