package org.mik.zoo.livingbeing.plants.flowers;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.plants.AbstractPlant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractFlowers extends AbstractPlant implements Flowers {

    public static final String FLOWER_COLOR ="flower_color";
    public static final String LEAF_NUM ="leaf_num";

    private static List<Flowers> allFlowers =new ArrayList<>();

    private int leafnum;
    private String flowercolor;

    public AbstractFlowers() {
        super();
        if (!allFlowers.contains(this))
            allFlowers.add(this);
    }

    public AbstractFlowers(String scientificName, String instanceName, String imageURI,
                            String flowercolor, int leafnum, int age, int height) {
        super(scientificName, instanceName, imageURI, age, height, ReproductionType.OVUM);
        this.leafnum = leafnum;
        this.flowercolor = flowercolor;
    }

    public AbstractFlowers(String scientificName) {
        super(scientificName, ReproductionType.OVUM);
    }

    public AbstractFlowers(ResultSet rs) throws SQLException {
            super(rs);
            this.leafnum =rs.getInt(FLOWER_COLOR);
            if (!allFlowers.contains(this))
                allFlowers.add(this);
    }

    @Override
    public int getLeafnum() {
        return leafnum;
    }
    @Override
    public String getFlowercolor() {
        return flowercolor;
    }

    public void setLeafnum(int leafnum) {
        this.leafnum = leafnum;
    }

    public boolean isFlower() {
        return true;
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d",super.getUpdateSql(), FLOWER_COLOR, getLeafnum());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(FLOWER_COLOR);
        values.append(',').append(getLeafnum());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractFlowers)) return false;
        if (!super.equals(o)) return false;
        AbstractFlowers that = (AbstractFlowers) o;
        return leafnum == that.leafnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), leafnum);
    }

    @Override
    public String toString() {
        return "AbstractFlowers{" +
                super.toString() +
                "leafnum=" + leafnum +
                "} ";
    }

    public static List<Flowers> getAllFlowers() {
        return allFlowers;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(FLOWER_COLOR, COL_TYPE_INTEGER);
    }
}
