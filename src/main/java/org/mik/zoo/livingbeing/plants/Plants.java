package org.mik.zoo.livingbeing.plants;

import org.mik.zoo.livingbeing.LivingBeing;

public interface Plants extends LivingBeing {

    int getNumberOfAge();

    int getNumberOfHeight();

}
