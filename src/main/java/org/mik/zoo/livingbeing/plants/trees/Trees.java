package org.mik.zoo.livingbeing.plants.trees;

import org.mik.zoo.livingbeing.plants.Plants;

public interface Trees extends Plants {

    int getthickness();
    boolean getevergreen();
}
