package org.mik.zoo.livingbeing.plants.flowers;

import org.mik.zoo.livingbeing.plants.Plants;

public interface Flowers extends Plants {

    int getLeafnum();
    String getFlowercolor();

}
