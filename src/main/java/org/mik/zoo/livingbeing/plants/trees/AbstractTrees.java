package org.mik.zoo.livingbeing.plants.trees;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.plants.AbstractPlant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractTrees extends AbstractPlant implements Trees {

    public static final String trunk_thickness ="trunk_thickness";
    public static final String isevergreen ="isevergreen";

    private static List<Trees> allTrees =new ArrayList<>();

    private int thickness;
    private boolean evergreen;

    public AbstractTrees() {
        super();
        if (!allTrees.contains(this))
            allTrees.add(this);
    }

    public AbstractTrees(String scientificName, String instanceName, String imageURI,
                         int thickness, boolean evergreen, int age, int height) {
        super(scientificName, instanceName, imageURI, age, height, ReproductionType.OVUM);
        this.evergreen = evergreen;
        this.thickness = thickness;
    }

    public AbstractTrees(String scientificName) {
        super(scientificName, ReproductionType.OVUM);
    }

    public AbstractTrees(ResultSet rs) throws SQLException {
            super(rs);
            this.thickness =rs.getInt(trunk_thickness);
            if (!allTrees.contains(this))
                allTrees.add(this);
    }

    @Override
    public int getthickness() {
        return thickness;
    }
    @Override
    public boolean getevergreen() {
        return evergreen;
    }

    public void setthickness(int thickness) {
        this.thickness = thickness;
    }

    public boolean isTree() {
        return true;
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d",super.getUpdateSql(), thickness, getthickness());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(trunk_thickness);
        values.append(',').append(getthickness());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractTrees)) return false;
        if (!super.equals(o)) return false;
        AbstractTrees that = (AbstractTrees) o;
        return thickness == that.thickness;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), thickness);
    }

    @Override
    public String toString() {
        return "AbstractTree{" +
                super.toString() +
                "thickness=" + thickness +
                "} ";
    }

    public static List<Trees> getAllTrees() {
        return allTrees;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(trunk_thickness, COL_TYPE_INTEGER);
    }
}
