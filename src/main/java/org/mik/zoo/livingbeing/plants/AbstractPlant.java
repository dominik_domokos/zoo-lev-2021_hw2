package org.mik.zoo.livingbeing.plants;

import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.ReproductionType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractPlant extends AbstractLivingBeing implements Plants {

    public static final String COL_NUM_OF_AGE ="num_of_age";
    public static final String COL_NUM_OF_HEIGHT ="num_of_height";

    private static List<Plants> allPlants =new ArrayList<>();

    private int numberOfAge;

    private int numberOfHeight;

    public AbstractPlant() {
        super();
        if (!allPlants.contains(this))
            allPlants.add(this);
    }

    public AbstractPlant(String scientificName, String instanceName, String imageURI,
                         int numberOfAge, int numberOfHeight, ReproductionType reproductionType) {
        super(scientificName,instanceName,imageURI, reproductionType);
        this.numberOfAge = numberOfAge;
        this.numberOfHeight = numberOfHeight;
    }

    public AbstractPlant(String scientificName, ReproductionType reproductionType) {
        super(scientificName, reproductionType);
    }

    public AbstractPlant(ResultSet rs) throws SQLException {
        super(rs);
        this.numberOfAge =rs.getInt(COL_NUM_OF_AGE);
        this.numberOfHeight =rs.getInt(COL_NUM_OF_HEIGHT);
        if (!allPlants.contains(this))
            allPlants.add(this);
    }

    @Override
    public boolean isPlant() {
        return true;
    }

    @Override
    public int getNumberOfAge() {
        return numberOfAge;
    }

    public void setNumberOfAge(int numberOfAge) {
        this.numberOfAge = numberOfAge;
    }

    @Override
    public int getNumberOfHeight() {
        return numberOfHeight;
    }

    public void setNumberOfHeight(int numberOfHeight) {
        this.numberOfHeight = numberOfHeight;
    }


    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d, %s=%d, %s=%d",super.getUpdateSql(),
                COL_NUM_OF_AGE, getNumberOfAge(),
                COL_NUM_OF_HEIGHT, getNumberOfHeight());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_NUM_OF_AGE).append(',')
              .append(COL_NUM_OF_HEIGHT);
        values.append(',').append(getNumberOfAge()).append(',')
              .append(getNumberOfHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractPlant)) return false;
        if (!super.equals(o)) return false;
        AbstractPlant that = (AbstractPlant) o;
        return numberOfAge == that.numberOfAge && numberOfHeight == that.numberOfHeight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numberOfAge, numberOfHeight);
    }

    @Override
    public String toString() {
        return "AbstractPlant{" +
                super.toString() +
                "numberOfAge=" + numberOfAge +
                ", numberOfHeight=" + numberOfHeight +
                "} " ;
    }

    public static List<Plants> getAllPlant() {
        return allPlants;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_NUM_OF_AGE, COL_TYPE_INTEGER);
        columnDefinitions.put(COL_NUM_OF_HEIGHT,COL_TYPE_INTEGER);
    }
}
