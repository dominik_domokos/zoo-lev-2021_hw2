package org.mik.zoo.livingbeing.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileFactory {

    private static final Logger LOG= LogManager.getLogger(FileFactory.class);
    private static final String FIELD_DELIMITER=";";

    private String fname;
    private int lines;
    private int errors;

    public FileFactory(String fname) {
        LOG.debug(String.format("Enter FileFactory fname:%s", fname));
        this.fname = fname;
    }

    public int getLines() {
        return lines;
    }

    public int getErrors() {
        return errors;
    }

    public boolean process() {
        this.lines=0;
        this.errors=0;
        try {
            for (String s: Files.readAllLines(Paths.get(this.fname))) {
                if (this.lines++==0)
                    continue;

                if (!processLine(s))
                    ++this.errors;
            }
            return true;
        }
        catch (Exception e) {
            LOG.warn(e);
            return false;
        }
    }

    private boolean processLine(String s) {
        LOG.debug(String.format("Enter processLine, s:%s", s));
        try(Scanner scanner=new Scanner(s)) {
            scanner.useDelimiter(FIELD_DELIMITER);
            if (!scanner.hasNext())
                return false;
            return createLivingBeing(scanner) != null;
        }
    }

    private LivingBeing createLivingBeing(Scanner s) {
        LOG.debug(String.format("Enter createLivingBeing"));
        try {
            if (!s.hasNext())
                return null;

            String tag=s.next();
            switch (tag) {
                case Elephant.TAG : return Elephant.factoryMethod(s);
                //case AfricanBuffalo.TAG : return  AfricanBuffalo.factoryMethod(s);

                default:
                    LOG.error(String.format("Unkown tag:%s", tag));
                    return null;
            }
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }
}
