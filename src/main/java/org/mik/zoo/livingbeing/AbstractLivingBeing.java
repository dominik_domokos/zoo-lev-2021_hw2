package org.mik.zoo.livingbeing;

import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.plants.AbstractPlant;
import org.mik.zoo.livingbeing.plants.flowers.AbstractFlowers;
import org.mik.zoo.livingbeing.plants.trees.AbstractTrees;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public abstract class AbstractLivingBeing implements LivingBeing {

    public static final String TABLE_NAME="living_being";
    public static final String COL_ID="id";
    public static final String COL_SCIENTIFIC_NAME="sc_name";
    public static final String COL_INSTANCE_NAME="ins_name";
    public static final String COL_URI="img_uri";

    public static final String COL_TYPE_VARCHAR_255 ="varchar(255)";
    public static final String COL_TYPE_INTEGER="integer";

    public static final int NUMBER_OF_CSV_ARGS = 2;

    protected static List<LivingBeing> allLivingBeing=new ArrayList<>();

    protected static Map<String,String> columnDefinitions=new HashMap<>();

    static {
        columnDefinitions.put(COL_ID, " integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY");
        columnDefinitions.put(COL_INSTANCE_NAME, COL_TYPE_VARCHAR_255);
        columnDefinitions.put(COL_INSTANCE_NAME, COL_TYPE_VARCHAR_255);
        columnDefinitions.put(COL_URI, COL_TYPE_VARCHAR_255);
        AbstractAnimal.setColumnDefinitions();
        AbstractMammal.setColumnDefinitions();
        //TODO
        AbstractPlant.setColumnDefinitions();
        AbstractFlowers.setColumnDefinitions();
        AbstractTrees.setColumnDefinitions();
    }



    private Integer id;
    private String scientificName;
    private String instanceName;
    private String imageURI;
    private ReproductionType reproductionType;

    public AbstractLivingBeing() {
        if (!allLivingBeing.contains(this))
            allLivingBeing.add(this);
    }

    public AbstractLivingBeing(String scientificName, String instanceName, String imageURI, ReproductionType reproductionType) {
        this();
        this.scientificName = scientificName;
        this.instanceName = instanceName;
        this.imageURI = imageURI;
        this.reproductionType=reproductionType;
    }

    public AbstractLivingBeing(String scientificName, ReproductionType reproductionType) {
        this(scientificName, null, null, reproductionType);
    }

    public AbstractLivingBeing(ResultSet rs) throws SQLException {
        this.id=Integer.valueOf(rs.getInt(COL_ID));
        this.scientificName=rs.getString(COL_SCIENTIFIC_NAME);
        this.instanceName=rs.getString(COL_INSTANCE_NAME);
        this.imageURI=rs.getString(COL_URI);
        if (!allLivingBeing.contains(this))
            allLivingBeing.add(this);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getScientificName() {
        return this.scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    @Override
    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    @Override
    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    @Override
    public ReproductionType getReproductionType() {
        return reproductionType;
    }

    @Override
    public boolean isAnimal() {
        return false;
    }

    @Override
    public boolean isBird() {
        return false;
    }

    @Override
    public boolean isFish() {
        return false;
    }

    @Override
    public boolean isFlower() {
        return false;
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public boolean isPlant() {
        return false;
    }

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        fields.append(COL_INSTANCE_NAME).append(',').append(COL_SCIENTIFIC_NAME).append(',').append(COL_URI);
        values.append('\'').append(getInstanceName()).append('\'').append(',')
                .append('\'').append(getScientificName()).append('\'').append(',')
                .append('\'').append(getImageURI()).append('\'');
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s=%d, %s='%s', %s='%s', %s='%s'",
                COL_ID,getId(),
                COL_INSTANCE_NAME, getInstanceName(),
                COL_SCIENTIFIC_NAME,getScientificName(),
                COL_URI, getImageURI());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractLivingBeing)) return false;
        AbstractLivingBeing that = (AbstractLivingBeing) o;
        return Objects.equals(scientificName, that.scientificName) && Objects.equals(instanceName, that.instanceName) && Objects.equals(imageURI, that.imageURI) && reproductionType == that.reproductionType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(scientificName, instanceName, imageURI, reproductionType);
    }

    @Override
    public String toString() {
        return "AbstractLivingBeing{" +
                "scientificName='" + scientificName + '\'' +
                ", instanceName='" + instanceName + '\'' +
                ", reproductionType=" + reproductionType +
                '}';
    }

    public static List<LivingBeing> getAllLivingBeings() {
        return allLivingBeing;
    }

    public static Map<String, String> getColumnDefinitions() {
        return columnDefinitions;
    }

    protected static String[] getArgs(Scanner s) {
        String[] result=new String[NUMBER_OF_CSV_ARGS];
        for (int i=0; s.hasNext() && i<NUMBER_OF_CSV_ARGS; i++)
            result[i]=s.next();

        return result;
    }
}
