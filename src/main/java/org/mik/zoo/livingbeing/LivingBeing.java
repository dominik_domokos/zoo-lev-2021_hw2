package org.mik.zoo.livingbeing;

public interface LivingBeing {

    Integer getId();

    void setId(Integer id);

    String getScientificName();

    String getInstanceName();

    String getImageURI();

    String getUpdateSql();

    void getInsertSql(StringBuilder fields, StringBuilder values);

    boolean isAnimal();

    boolean isPlant();

    boolean isTree();

    boolean isFlower();

    boolean isMammal();

    boolean isFish();

    boolean isBird();

    ReproductionType getReproductionType();

}
