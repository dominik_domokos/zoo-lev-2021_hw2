package org.mik.zoo.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.mammal.AfricanBuffalo;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;
import org.mik.zoo.livingbeing.plants.flowers.Sunflower;
import org.mik.zoo.livingbeing.plants.trees.Oaktree;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ZooDao implements Dao {

    private static final Logger LOG = LogManager.getLogger();

    private static ZooDao instance=null;

    private Connection connection;

    private ZooDao() {
        try {
            open();
            //initDb();
        }catch(Exception e){
            System.out.println(e.toString());
        }

    }

    @Override
    public void close() {
        LOG.debug("Enter close");
        try {
            this.connection.close();
        }
        catch (Exception e) {
            LOG.error("Close error:"+e);
        }
    }

    private void connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        //String url="jdbc:hsqldb:mem:zoodb;hsqldb.log_data=false";
        String url="jdbc:hsqldb:file:zoodao";
        this.connection= DriverManager.getConnection(url, "sa", "");
    }

    private String createColumnDefinitions() {
        LOG.debug("Enter createColumnDefinitions");
        StringBuilder sb=new StringBuilder();
        for (Map.Entry<String,String> entry:AbstractLivingBeing.getColumnDefinitions().entrySet()) {
            sb.append(entry.getKey()).append(' ').append(entry.getValue()).append(',');
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    protected String getInsertSql(LivingBeing lb) {
        StringBuilder fields=new StringBuilder();
        StringBuilder values=new StringBuilder();
        lb.getInsertSql(fields, values);
        return String.format("insert into %s (%s) values (%s)", AbstractLivingBeing.TABLE_NAME, fields.toString(), values.toString());
    }

    public LivingBeing insert(LivingBeing lb) throws SQLException {
        LOG.debug("Enter insert:"+lb);
        if (!open())
            return null;

        String sql=getInsertSql(lb);
        try(Statement stmt=this.connection.createStatement()) {
            if (stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS) !=1)
                return null;

            try(ResultSet generatedKeys=stmt.getGeneratedKeys()) {
                if (generatedKeys.next())
                    lb.setId(Integer.valueOf(generatedKeys.getInt(1)));
                else
                    return null;
            }
            return lb;
        }
    }

    private void initDb() throws SQLException{
        insert(new Elephant("Jumbo"));
        insert(new Elephant("Dumbo"));
        insert(new AfricanBuffalo("Mike"));
        insert(new AfricanBuffalo("Sandy"));

        insert(new Oaktree("Kew"));
        insert(new Oaktree("Dew"));
        insert(new Sunflower("Sunny"));
        insert(new Sunflower("Sunner"));
    }

    private void checkTable() throws SQLException {
        LOG.debug("Enter checkTable");

        DatabaseMetaData meta=this.connection.getMetaData();
        try (ResultSet rs=meta.getTables(null, null, AbstractLivingBeing.TABLE_NAME, null)) {
            while(rs.next()) {
                String tn=rs.getString("TABLE_NAME");
                if (tn!=null && tn.equals(AbstractLivingBeing.TABLE_NAME))
                    return;
            }
        }

        LOG.info("Database is empty");
        try (Statement stmt=this.connection.createStatement()) {
            String sql=String.format("create table %s (%s);", AbstractLivingBeing.TABLE_NAME, createColumnDefinitions());
            stmt.executeUpdate(sql);
        }
        initDb();
    }

    protected boolean open() {
        LOG.debug("Enter open");
        if (this.connection!=null)
            return true;
        try {
            connect();
            checkTable();
            LOG.info("Databse successfuly opened");
            return true;
        }
        catch (Exception e) {
            LOG.error("Open error: "+e);
            return false;
        }
    }

    @Override
    public LivingBeing getById(Integer id) {
        LOG.debug("Enter getById, id:"+id);
        if (id==null)
            return null;

        if (!open())
            return null;

        String sql=String.format("select * from %s where id=%d;", AbstractLivingBeing.TABLE_NAME, id);
        try (Statement stmt=this.connection.createStatement()) {
            try (ResultSet rs=stmt.executeQuery(sql)) {
                String sn = rs.getString(AbstractLivingBeing.COL_SCIENTIFIC_NAME);
                return createInstance(rs, sn);
            }
        }
        catch (Exception e) {
            LOG.error("GetById error:"+e);
        }
        return null;
    }

    protected LivingBeing createInstance(ResultSet rs, String scientificName) throws SQLException {
        switch (scientificName) {
            case Elephant.SCIENTIFIC_NAME: return new Elephant(rs);
            //..
            default:
                LOG.error(String.format(String.format("Unknown scientificName in createInstance:%s", scientificName)));
                return null;
        }
    }

    @Override
    public List<LivingBeing> getAll() {
        LOG.debug("Enter getAll");
        List<LivingBeing> result=new ArrayList<>();
        if (!open())
            return result;
        String sql=String.format("select * from %s", AbstractLivingBeing.TABLE_NAME);
        try (Statement stmt=this.connection.createStatement()) {
            try(ResultSet rs=stmt.executeQuery(sql)) {
                addResultList(rs, result);
                return result;
            }
        }
        catch (Exception e) {
            LOG.error("getAll error:"+e);
            return result;
        }
    }

    protected void addResultList(ResultSet re, List<LivingBeing> result) throws SQLException {
        while (re.next()){
            String scientificName=re.getString(AbstractLivingBeing.COL_SCIENTIFIC_NAME);
            LivingBeing lb=createInstance(re, scientificName);
            if (lb!=null)
                result.add(lb);
        }
    }

    protected List<LivingBeing> findByColumn(String colName, Object ...args) {
        LOG.debug("Enter findByColumn, columname:"+colName);
        List<LivingBeing> result=new ArrayList<>();
        if (!open())
            return result;
        String sql=String.format("Select * from %s where %s='%s'", AbstractLivingBeing.TABLE_NAME,
                colName, args);
        try(Statement stmt=this.connection.createStatement()) {
            try(ResultSet rs=stmt.executeQuery(sql)){
                addResultList(rs, result);
                return result;
            }
        }
        catch (Exception e) {
            LOG.error("findByColumn error:"+e);
            return result;
        }
    }

    @Override
    public List<LivingBeing> findByScientificName(String name) {
        LOG.debug("Enter findByScientificName, name:"+name);
        return findByColumn(AbstractLivingBeing.COL_SCIENTIFIC_NAME, name);
    }

    @Override
    public List<LivingBeing> findByInstanceName(String name) {
        LOG.debug("Enter findByInstanceName, name:"+name);
        return findByColumn(AbstractLivingBeing.COL_INSTANCE_NAME, name);
    }

    @Override
    public boolean delete(LivingBeing lb) {
        LOG.debug("Enter delete, lb:"+lb);
        if (!open())
            return false;

        String sql=String.format("delete from %s where id=%d", AbstractLivingBeing.TABLE_NAME, lb.getId());
        try(Statement stmt=this.connection.createStatement()) {
            return stmt.executeUpdate(sql)==1;
        }
        catch (Exception e) {
            LOG.error(String.format("Delete error:%d", e.getMessage()));
            return false;
        }
    }

    protected  String getUpdateSql(LivingBeing lb) {
        return String.format("update %s set %s where %s=%d", AbstractLivingBeing.TABLE_NAME, lb.getUpdateSql(),
                        AbstractLivingBeing.COL_ID, lb.getId());
    }

    protected LivingBeing update(LivingBeing lb) throws SQLException {
        LOG.debug("Enter update:"+lb);
        if (!open())
            return null;
        String sql=getUpdateSql(lb);
        try (Statement stmt=this.connection.createStatement()) {
            return stmt.executeUpdate(sql)==1 ? lb : null;
        }
    }

    @Override
    public LivingBeing persist(LivingBeing lb) {
        LOG.debug("Enter persist, lb:"+lb);
        try {
            return lb.getId() == null ? insert(lb) : update(lb);
        }
        catch (Exception e) {
            LOG.error("persist error:"+e);
            return null;
        }
    }

    public static synchronized ZooDao getInstance() {
        if (instance==null)
            instance = new ZooDao();
        return instance;
    }
}
