package org.mik.zoo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.db.ZooDao;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.plants.AbstractPlant;

import java.util.List;
import java.util.stream.Collectors;

public class ZooApplication {

    private static final Logger LOG = LogManager.getLogger(ZooApplication.class.getName());

    private ZooDao dao=ZooDao.getInstance();

    private void query() {
        System.out.println(String.format("Number of all living beings:%d", AbstractLivingBeing.getAllLivingBeings().size()));
        System.out.println(String.format("Number of all animals:%d", AbstractAnimal.getAllAnimal().size()));
        System.out.println(String.format("Number of all plants:%d", AbstractPlant.getAllPlant().size()));

        for (LivingBeing lb:AbstractLivingBeing.getAllLivingBeings()) {
            if (lb.getInstanceName().equals("Jumbo"))
                System.out.println("Jumbo is exists"+lb);
            if (lb.getInstanceName().equals("Sunner"))
                System.out.println("Sunner is exists"+lb);
        }

        List<LivingBeing> jumbos=
                AbstractLivingBeing.getAllLivingBeings().stream().filter(lb-> lb.getInstanceName().equals("Jumbo"))
                        .collect(Collectors.toList());
        if (jumbos.size()==0)
            System.out.println("Sorry we don't have any Jumbo");
        jumbos.forEach(lb->System.out.println("Jumbo:"+lb));

        //plants
        List<LivingBeing> plants=
                AbstractLivingBeing.getAllLivingBeings().stream().filter(lb-> lb.getInstanceName().equals("Sunner"))
                        .collect(Collectors.toList());
        if (plants.size()==0)
            System.out.println("Sorry we don't have any Sunner");
        plants.forEach(lb->System.out.println("Sunner:"+lb));
    }

    public ZooApplication(){
        query();
    }

    public static void main(String args[]) {
        new ZooApplication();
    }
 }
